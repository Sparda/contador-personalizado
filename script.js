function contar() {
    var ini = Number(window.document.getElementById('ini').value)
    var fim = Number(window.document.getElementById('fim').value)
    var passo = Number(window.document.getElementById('passo').value)
    var res = window.document.getElementById('res')
    var distance = fim - ini
    res.innerHTML = ``
    if (distance == 0) {
        window.alert('Você não se moveu!')
    }
    if (distance < 0) {
        for (var i = ini; i > fim; i -= passo) {
            if (i == ini) {
                res.innerHTML += `${i} `
            } else {
                res.innerHTML += `&#128073; ${i} `
            }

        }
        res.innerHTML += `&#128073; &#127937;`

    } else {
        for (var i = ini; i < fim; i += passo) {
            if (i == ini) {
                res.innerHTML += `${i} `
            } else {
                res.innerHTML += `&#128073; ${i} `
            }

        }
        res.innerHTML += `&#128073; &#127937;!`
    }
}